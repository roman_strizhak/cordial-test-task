package qa.task.cordial.steps;

import java.util.List;

import qa.task.cordial.webservices.api.SortRestClient;
import qa.task.cordial.webservices.dto.NumbersDto;
import ru.yandex.qatools.allure.annotations.Step;

public class SortApiSteps {

    private final SortRestClient sortRestClient;

    public SortApiSteps() {
        sortRestClient = new SortRestClient();
    }

    @Step
    public List<Long> sortNumbersList(final List<Long> unsortedNumbers) {
        var unsortedNumbersDto = new NumbersDto().numbers(unsortedNumbers);
        var sortedNumbersDto = sortRestClient.sortNumbers(unsortedNumbersDto).then().extract().body()
                .as(NumbersDto.class);
        return sortedNumbersDto.numbers();
    }

}