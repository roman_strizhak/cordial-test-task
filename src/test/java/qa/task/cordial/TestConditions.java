package qa.task.cordial;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TestConditions {

    @BeforeSuite
    public void beforeSuite() {
        log.info("TestSuite started");
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        log.info("TestSuite finished");
    }
}