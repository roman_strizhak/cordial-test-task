package qa.task.cordial.tests;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;

import org.testng.annotations.Test;

import qa.task.cordial.TestConditions;
import qa.task.cordial.steps.SortApiSteps;
import ru.yandex.qatools.allure.annotations.Severity;
import ru.yandex.qatools.allure.model.SeverityLevel;

import static org.assertj.core.api.Assertions.assertThat;

public class SortNumbersApiTests extends TestConditions {

    private final SortApiSteps sortApiSteps = new SortApiSteps();

    @Test(priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void sortPositiveNumbersList() {
        // given
        var unsortedPositiveNumbersList = Arrays.asList(9L, 51L, 1L, 45L, 999L);
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(unsortedPositiveNumbersList);
        // then
        assertThat(sortedNumbersList).as("Sorted Positive Numbers List").isSorted()
                .hasSameSizeAs(unsortedPositiveNumbersList)
                .containsExactlyInAnyOrderElementsOf(unsortedPositiveNumbersList);
    }

    @Test(priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void sortNegativeNumbersList() {
        // given
        var unsortedNegativeList = Arrays.asList(-100L, -9L, -1L, -553L, -999L);
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(unsortedNegativeList);
        // then
        assertThat(sortedNumbersList).as("Sorted Negative Numbers List").isSorted().hasSameSizeAs(unsortedNegativeList)
                .containsExactlyInAnyOrderElementsOf(unsortedNegativeList);
    }

    @Test(priority = 1)
    @Severity(SeverityLevel.CRITICAL)
    public void sortMixedNumbersList() {
        // given
        var unsortedMixedList = Arrays.asList(123L, -3L, 0L, 77L, -999L, 12L, -44L, 999L, 567L, -8L);
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(unsortedMixedList);
        // then
        assertThat(sortedNumbersList).as("Sorted Mixed Numbers List").isSorted().hasSameSizeAs(unsortedMixedList)
                .containsExactlyInAnyOrderElementsOf(unsortedMixedList);
    }

    @Test(priority = 2)
    @Severity(SeverityLevel.NORMAL)
    public void sortSingleItemList() {
        // given
        var singleItemList = Collections.singletonList(1L);
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(singleItemList);
        // then
        assertThat(sortedNumbersList).isEqualTo(singleItemList);
    }

    @Test(priority = 2)
    @Severity(SeverityLevel.NORMAL)
    public void sortBigSizeList() {
        // given
        var unsortedNumbersList = new ArrayList<Long>();
        var unsortedNumbersListSize = 10000;
        // add random numbers to list
        for (int i = 0; i < unsortedNumbersListSize; i++) {
            unsortedNumbersList.add(new Random().nextLong());
        }
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(unsortedNumbersList);
        // then
        assertThat(sortedNumbersList).as("Sorted Numbers List with big size").isSorted()
                .hasSameSizeAs(unsortedNumbersList).containsExactlyInAnyOrderElementsOf(unsortedNumbersList);
    }

    @Test(priority = 2)
    @Severity(SeverityLevel.NORMAL)
    public void sortBigNumberValuesList() {
        // given
        var unsortedNumbersList = Arrays.asList(Long.MAX_VALUE, Long.MIN_VALUE, -223372036854775808L,
                23372036854775808L, 3372036854775808L, -372036854775808L, -72036854775808L, 2036854775808L,
                136854775808L, -36854775808L, 68547758080L, -854775808L, 54775808L);
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(unsortedNumbersList);
        // then
        assertThat(sortedNumbersList).as("Sorted Numbers List with big values").isSorted()
                .hasSameSizeAs(unsortedNumbersList).containsExactlyInAnyOrderElementsOf(unsortedNumbersList);
    }

    @Test(priority = 2)
    @Severity(SeverityLevel.NORMAL)
    public void sortDuplicateValuesList() {
        // given
        var unsortedNumbersList = Arrays.asList(-2222L, 1L, 33333L, 9999999999L, -2222L, 33333L);
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(unsortedNumbersList);
        // then
        assertThat(sortedNumbersList).as("Sorted Numbers List with duplicated values").isSorted()
                .hasSameSizeAs(unsortedNumbersList).containsExactlyInAnyOrderElementsOf(unsortedNumbersList);
    }

    @Test(enabled = false, priority = 3, description = "need clarify requirements for empty list processing")
    @Severity(SeverityLevel.TRIVIAL)
    public void sortEmptyList() {
        // given
        var emptyList = new ArrayList<Long>();
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(emptyList);
        // then
        assertThat(sortedNumbersList).as("Empty List").isEmpty();
    }

    @Test(priority = 3, description = "need clarify requirements for NULL processing")
    @Severity(SeverityLevel.TRIVIAL)
    public void sortNumbersListWithNullValue() {
        // given
        var unsortedNumbersListWithNull = Arrays.asList(5L, 66L, null, 777L, 76L);
        var expectedSortedListWithNull = Arrays.asList(null, 5L, 66L, 76L, 777L);
        // when
        var sortedNumbersList = sortApiSteps.sortNumbersList(unsortedNumbersListWithNull);
        // then
        assertThat(sortedNumbersList).as("Sorted Numbers List with NULL").isEqualTo(expectedSortedListWithNull);
    }

}