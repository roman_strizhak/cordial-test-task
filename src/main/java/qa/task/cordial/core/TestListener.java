package qa.task.cordial.core;

import org.testng.ITestResult;
import org.testng.TestListenerAdapter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public final class TestListener extends TestListenerAdapter {

    @Override
    public void onTestStart(final ITestResult testResult) {
        log.info("START TEST: {} ({})", testResult.getName(), testResult.getInstanceName());
    }

    @Override
    public void onTestSuccess(final ITestResult testResult) {
        log.info("TEST PASSED :) - ({} milliseconds)\n-------------------------------------------------------",
                getDuration(testResult));
    }

    @Override
    public void onTestSkipped(final ITestResult testResult) {
        log.info("TEST SKIPPED :| - {}\n-------------------------------------------------------", testResult.getName());
    }

    @Override
    public void onTestFailure(final ITestResult testResult) {
        log.error("TEST FAILED :( - ({} milliseconds)\n-------------------------------------------------------",
                getDuration(testResult));
    }

    private long getDuration(final ITestResult testResult) {
        return testResult.getEndMillis() - testResult.getStartMillis();
    }
}