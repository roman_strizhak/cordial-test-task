package qa.task.cordial.core.properties;

import org.aeonbits.owner.Config;

@Config.Sources({ "classpath:test_config.properties" })
public interface ITestConfig extends Config {

    @Key("base.url")
    String baseUrl();

}