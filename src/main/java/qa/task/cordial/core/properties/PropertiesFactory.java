package qa.task.cordial.core.properties;

import java.util.Objects;

import org.aeonbits.owner.ConfigFactory;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PropertiesFactory {

    private static ITestConfig iTestConfig;

    public static ITestConfig getTestConfig() {
        if (Objects.isNull(iTestConfig)) {
            iTestConfig = ConfigFactory.create(ITestConfig.class);
        }
        return iTestConfig;
    }
}
