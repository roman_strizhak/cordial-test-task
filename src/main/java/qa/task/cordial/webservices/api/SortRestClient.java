package qa.task.cordial.webservices.api;

import io.restassured.response.Response;
import qa.task.cordial.webservices.BaseWebservice;
import qa.task.cordial.webservices.dto.NumbersDto;

public class SortRestClient extends BaseWebservice {

    private static final String SORT_SUFFIX = "/api/sort";

    public Response sortNumbers(final NumbersDto numbersDto) {
        return getJsonRestClient().request().with().body(numbersDto).post(uri(SORT_SUFFIX));
    }

}
