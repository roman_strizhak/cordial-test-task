package qa.task.cordial.webservices;

import java.net.URI;

import io.restassured.RestAssured;
import io.restassured.filter.log.ErrorLoggingFilter;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import qa.task.cordial.core.properties.PropertiesFactory;

public class BaseWebservice {

    private static final String ACCEPT_JSON = "application/json, text/plain, */*";

    protected RequestSpecification getJsonRestClient() {
        return getBaseRestClient().contentType(ContentType.JSON).accept(ACCEPT_JSON);
    }

    protected URI uri(final String path) {
        return URI.create(path);
    }

    private RequestSpecification getBaseRestClient() {
        var baseUri = PropertiesFactory.getTestConfig().baseUrl();
        return RestAssured.given().baseUri(baseUri).filters(new ErrorLoggingFilter(),
                new RequestLoggingFilter(LogDetail.METHOD), new RequestLoggingFilter(LogDetail.URI),
                new RequestLoggingFilter(LogDetail.BODY, false, System.out),
                new ResponseLoggingFilter(LogDetail.BODY, false, System.out),
                new ResponseLoggingFilter(LogDetail.STATUS));
    }
}