package qa.task.cordial.webservices.dto;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true, fluent = true)
public class NumbersDto {

    @SerializedName("numbers")
    private List<Long> numbers;

}
