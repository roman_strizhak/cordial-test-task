package qa.task.cordial.utils;

import lombok.experimental.UtilityClass;
import ru.yandex.qatools.allure.annotations.Attachment;

@UtilityClass
public class AllureReportUtil {

    @Attachment("Result")
    public Object attachDataToReport(final Object data) {
        return data;
    }
}
