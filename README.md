## Required software

**The following software is required:**

1. Java JDK 11 or above
2. Apache Maven 3.8.1 or above
3. Git
4. [optional] Lombok Plugin for building project with tests in IntelliJ IDEA
-----------------


## Working with project

**Download**
```
$ git clone git clone https://roman_strizhak@bitbucket.org/roman_strizhak/cordial-test-task.git
```

** [optional] Init 'idea-plugins-settings' submodule with IDEA CheckStyle & Formatter plugin settings**
```
$ git submodule update --init --recursive 
```

## Run tests
```
$ mvn integration-test
```

## Generate report
```
$ mvn allure:serve
```
Allure report will be generated from test artefacts at target/allure-results directory and automatically opened in default browser.
